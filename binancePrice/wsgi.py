"""
WSGI config for binancePrice project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/2.2/howto/deployment/wsgi/
"""

import os
import threading


from django.core.wsgi import get_wsgi_application
from api.worker import update_prices

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'binancePrice.settings')

application = get_wsgi_application()

threading.Thread(target=update_prices).start()

from pymongo import MongoClient
import requests
import time
import datetime
import logging

logger = logging.getLogger(__name__)


def update_prices():
    mongo_client = MongoClient()
    db = mongo_client['Test']
    prices = db['prices']
    logger.info('start updating prices')
    while True:
        r = requests.get('https://api.binance.com/api/v3/ticker/price')
        response_result = r.json()
        data = {
            'time': int(str(datetime.datetime.now().timestamp()).split('.')[0]),
            'prices': response_result
        }
        prices.insert_one(data)
        time.sleep(5)

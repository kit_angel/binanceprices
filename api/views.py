from django.http import JsonResponse
from django.shortcuts import render
from django.views.decorators.cache import cache_page
from django.http import HttpResponseNotAllowed
from django.views.decorators.csrf import csrf_exempt
import logging
from pymongo import MongoClient
import datetime
import pymongo

logger = logging.getLogger(__name__)

mongo_client = MongoClient()
db = mongo_client['Test']
prices = db['prices']


def index(request):
    return render(request, 'api/index.html')


@csrf_exempt
@cache_page(2)
def price(request):
    if request.method != 'GET':
        return HttpResponseNotAllowed('not allowed')

    paused_param = request.GET.get('pause', '')
    date_param = request.GET.get('date', '')

    pause_symbols = paused_param.split(',')

    # фильтруем по выбрабнной дате
    if date_param:
        timestamp = int(date_param)
        response_result = None
        now = int(str(datetime.datetime.now().timestamp()).split('.')[0])
        while response_result is None or now > timestamp:
            response_result = prices.find_one({'time': int(timestamp)})
            timestamp += 3  # если не найдено, то ищем за следующую секунду (ближайшие данные)
    else:  # если фильтра по дате нет, то берем последние данные
        response_result = prices.find_one(sort=[('_id', pymongo.DESCENDING)])

    result = []

    # если данных нет
    if response_result is None:
        data = {
            'result': None
        }
        return JsonResponse(data)

    # если данные есть, то фильтруем
    for item in response_result.get('prices'):
        if item['symbol'] in pause_symbols:
            continue
        else:
            result.append(item)
    data = {
        'result': result
    }
    return JsonResponse(data)
